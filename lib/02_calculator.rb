def add(num1, num2)
  return num1 + num2
end

def subtract(num1, num2)
  return num1 - num2
end

def sum(array_of_nums)
  total = 0
  for num in array_of_nums
    total += num
  end
  return total
end

def multiply(array_of_nums)
  total = 1
  for num in array_of_nums
    total *= num
  end
  return total
end

def power(base, exponent)
  result = 1
  for idx in (0...exponent)
    result *= base
  end
  return result
end

def factorial(num)
  if num == 0
    return 1
  else
    result = num
    for idx in (1...num)
      result *= idx
    end
    return result
  end
end
