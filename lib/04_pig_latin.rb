def is_vowel?(letter)
  vowels = ["a","e","i","o","u"]
  if vowels.include?(letter)
    return true
  end
  false
end

def three_cons?(three_chr_string)
  three_cons_array = ["thr","sch","scr", "shr", "spl", "spr", "squ", "str"]
  if three_cons_array.include?(three_chr_string)
    return true
  end
  false
end

def two_cons?(two_chr_string)
  two_cons_array = ["bl", "br", "ch", "ck", "cl", "cr", "dr", "fl", "fr", 
    "gh", "gl", "gr", "ng", "ph", "pl", "pr", "qu", "sc", "sh", "sk", "sl",
     "sm", "sn", "sp", "st", "sw", "th", "tr", "tw", "wh", "wr"]
  if two_cons_array.include?(two_chr_string)
    return true
  end
  false
end

def translate_word(word)
  if is_vowel?(word[0])
    return word + "ay"
  elsif three_cons?(word[0..2])
    return word[3...word.length] + word[0..2] + "ay"
  elsif two_cons?(word[0..1])
    return word[2...word.length] + word[0..1] + "ay"
  else
    return word[1...word.length] + word[0] + "ay"
  end
end

def translate(sentence)
  result = []
  for word in sentence.split(" ")
    result << translate_word(word)
  end
  result.join(" ")
end
